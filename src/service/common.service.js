const R = require('ramda');

const pg = require('../core/pg.core');

module.exports.insertOne = R.curry(
  (fGetStringKeys, fValuesIndex, table, data) =>
    pg.query({
      text: `INSERT INTO ${table} (${fGetStringKeys(
        data
      )}) VALUES (${fValuesIndex(data)}) RETURNING *`,
      values: Object.values(data),
    })
);

module.exports.findOne = R.curry((fConditions, table, conditions) =>
  pg.query({
    text: `SELECT *FROM ${table} WHERE (${fConditions(conditions)}) LIMIT 1`,
    values: Object.values(conditions),
  })
);

module.exports.crypto = f => x => y => f(x, y);

module.exports.jwt = f => x => y => z => f(x, y, z);
