const Router = require('koa-router');
const validator = require('../middlewares/validator');
const schema = require('./chema');
const handler = require('./handler');

const router = new Router();

router.post('/register', validator(schema.register), handler.register);
router.post('/login', validator(schema.login), handler.login);

// ENV DEVELOMENT
router.post('/hashPassword', validator(schema.hashPaswword), handler.hashPaswword);


module.exports = router;
