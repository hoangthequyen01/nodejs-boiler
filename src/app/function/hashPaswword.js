const userSvc = require('../../service/user.service');

module.exports = async ctx => {
  try {
    const { password } = ctx.request.body;

    const hashPawd = await userSvc.hashPaswword(password);

    ctx.status = 200;
    ctx.body = {
      password: hashPawd,
    };
  } catch (error) {
    ctx.throw(error);
  }
};
