const fs = require('fs');
const config = require('config');
const path = require('path');

// const file = fs.readFileSync(path.resolve(__dirname, '../file.xml'));

module.exports.privateKey = fs.readFileSync(
  path.resolve(__dirname, config.get('privateKeyPath')),
  'utf8'
);

module.exports.publicKey = fs.readFileSync(
  path.resolve(__dirname, config.get('publicKeyPath')),
  'utf8'
);
