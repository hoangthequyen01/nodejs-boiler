#!/usr/bin/env bash

# Push GitLab CI/CD build status to GITLAB Cloud

if [ -z "$GITLAB_ACCESS_TOKEN" ]; then
   echo "ERROR: GITLAB_ACCESS_TOKEN is not set"
exit 1
fi
if [ -z "$GITLAB_USERNAME" ]; then
    echo "ERROR: GITLAB_USERNAME is not set"
exit 1
fi
if [ -z "$GITLAB_NAMESPACE" ]; then
    echo "Setting GITLAB_NAMESPACE to $CI_PROJECT_NAMESPACE"
    GITLAB_NAMESPACE=$CI_PROJECT_NAMESPACE
fi
if [ -z "$GITLAB_REPOSITORY" ]; then
    echo "Setting GITLAB_REPOSITORY to $CI_PROJECT_NAME"
    GITLAB_REPOSITORY=$CI_PROJECT_NAME
fi

GITLAB_API_ROOT="https://api.GITLAB.org/2.0"
GITLAB_STATUS_API="$GITLAB_API_ROOT/repositories/$GITLAB_NAMESPACE/$GITLAB_REPOSITORY/commit/$CI_COMMIT_SHA/statuses/build"
GITLAB_KEY="ci/gitlab-ci/$CI_JOB_NAME"

case "$BUILD_STATUS" in
running)
   GITLAB_STATE="INPROGRESS"
   GITLAB_DESCRIPTION="The build is running!"
   ;;
passed)
   GITLAB_STATE="SUCCESSFUL"
   GITLAB_DESCRIPTION="The build passed!"
   ;;
failed)
   GITLAB_STATE="FAILED"
   GITLAB_DESCRIPTION="The build failed."
   ;;
esac

echo "Pushing status to $GITLAB_STATUS_API..."
curl --request POST $GITLAB_STATUS_API \
--user $GITLAB_USERNAME:$GITLAB_ACCESS_TOKEN \
--header "Content-Type:application/json" \
--silent \
--data "{ \"state\": \"$GITLAB_STATE\", \"key\": \"$GITLAB_KEY\", \"description\":
\"$GITLAB_DESCRIPTION\",\"url\": \"$CI_PROJECT_URL/-/jobs/$CI_JOB_ID\" }"